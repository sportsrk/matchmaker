﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRkMatchmaker
{
    public struct MyResponse
    {
        public List<MatchOTD> Matches { get; set; }
        public List<string> UnmatchedIds { get; set; }

        public MyResponse(List<MatchOTD> matches, List<string> unmatchedIds)
        {
            Matches = matches;
            UnmatchedIds = unmatchedIds;
        }
    }
}
